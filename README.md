# bowtie2 Singularity container
### Bionformatics package bowtie2<br>
Bowtie 2 is an ultrafast and memory-efficient tool for aligning sequencing reads to long reference sequences. It is particularly good at aligning reads of about 50 up to 100s or 1,000s of characters, and particularly good at aligning to relatively long (e.g. mammalian) genomes<br>
bowtie2 Version: 2.4.1<br>
[http://bowtie-bio.sourceforge.net/bowtie2/index.shtml]

Singularity container based on the recipe: Singularity.bowtie2_v2.4.1

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build bowtie2_v2.4.1.sif Singularity.bowtie2_v2.4.1`

### Get image help
`singularity run-help ./bowtie2_v2.4.1.sif`

#### Default runscript: STAR
#### Usage:
  `bowtie2_v2.4.1.sif --help`<br>
    or:<br>
  `singularity exec bowtie2_v2.4.1.sif bowtie2 --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull bowtie2_v2.4.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/bowtie2/bowtie2:latest`


